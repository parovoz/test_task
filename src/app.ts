import {module} from 'angular'
import {Component} from './date.component.ts'
import moment from 'moment/src/moment.js'

class AppController implements ng.IController {
  date1:string;
  date2:string;
  changeDates () {
    alert((this.date1 || null) + '\n' + (this.date2 || null));
  }
  constructor () {

  }
}

export let app = module('myApp', ['ngMaterial'])
.controller('appController', AppController)
.component('mcDate', new Component())
.config(['$mdDateLocaleProvider', function ($mdDateLocaleProvider) {
  $mdDateLocaleProvider.formatDate = function (date) {
    let mdate = moment(date);
    return mdate.isValid() ? mdate.format('DD.MM.YYYY') : '';
  }
  $mdDateLocaleProvider.parseDate = function(dateString) {
      let m = moment(dateString, 'L', true);
      return m.isValid() ? m.toDate() : null;
  };
}]);
