import moment from 'moment/src/moment.js'

class Controller implements ng.IController {

  _maxDate:Date;
  _minDate:Date;
  hasChanges:boolean;
  _dateFrom:string = null;
  _dateTo:string = null;
  mcChange:any;

  changeDate () {
    if (this.mcChange && this.hasChanges) {
      this.hasChanges = false;
      setTimeout(this.mcChange, 100); //Sorry :)
    }
  }

  set minDate(value: Date) {
      this._minDate = value;
  }

  set maxDate(value: Date) {
      this._maxDate = value;
  }

  get minDate(): Date {
      return this._minDate;
  }

  get maxDate(): Date {
      return this._maxDate;
  }

  set dateTo(value:string) {
    if (!(new Date(this._dateFrom) <= new Date(value)) && this._dateFrom && value !== '')
      return;
    if (moment(this._dateTo).format('YYYY-MM-DD') != moment(value).format('YYYY-MM-DD')) this.hasChanges = true;
    this._dateTo = value;
    this.maxDate = value === '' ? null : new Date(value);
  }

  get dateTo():string {
    let date = moment(this._dateTo);
    return date.isValid() ? date.format('YYYY-MM-DD') : '';
  }

  set dateFrom(value:string) {
    if (!(new Date(value) <= new Date(this._dateTo)) && this.dateTo && value !== '')
      return;
    if (moment(this._dateFrom).format('YYYY-MM-DD') != moment(value).format('YYYY-MM-DD')) this.hasChanges = true;
    this._dateFrom = value;
    this.minDate = value === '' ? null : new Date(value);
  }

  get dateFrom():string {
    let date = moment(this._dateFrom);
    return date.isValid() ? date.format('YYYY-MM-DD') : '';
  }

  setYesterday () {
    this.dateTo = moment().subtract(1, 'day');
    this.dateFrom = moment().subtract(1, 'day');
    this.changeDate();
  }

  setMonth () {
    this.dateTo = moment();
    this.dateFrom = moment().subtract(30, 'day');
    this.changeDate();
  }

  setToday () {
    this.dateTo = moment();
    this.dateFrom = moment();
    this.changeDate();
  }

  setTwoWeek () {
    this.dateTo = moment();
    this.dateFrom = moment().subtract(14, 'day');
    this.changeDate();
  }

  setAll () {
    if (this._dateTo != null || this._dateFrom != null) this.hasChanges = true;
    this.minDate = null;
    this.maxDate = null;
    this._dateFrom = null;
    this._dateTo = null;
    this.changeDate();
  }

  constructor () {
      console.log('start controller');
      this.minDate = new Date();
      this.maxDate = new Date();
      this.dateFrom = moment();
      this.dateTo = moment();
  }
}

export class Component implements ng.IComponentOptions {
  static NAME:string = 'mcDate';
  template:any;
  controller:any;
  bindings:any;
  constructor () {
    this.controller = Controller;
    this.bindings = {
      dateFrom: '=',
      dateTo: '=',
      mcChange: '&'
    }
    this.template = `
    <div class="component-wrapper">
      <div class="datepicker-wrapper">
        <div class="date-item">
          <div>С</div>
          <md-datepicker ng-model=$ctrl.dateFrom md-max-date=$ctrl.maxDate ng-required="" ng-model-options="{getterSetter: true, allowInvalid: true}" ng-change=$ctrl.changeDate()></md-datepicker>
        </div>
        <div class="date-item">
          <div>По</div>
          <md-datepicker ng-model=$ctrl.dateTo md-min-date=$ctrl.minDate ng-required="" ng-model-options="{getterSetter: true, allowInvalid: true}" ng-change=$ctrl.changeDate()></md-datepicker>
        </div>
      </div>
      <div class="preset-wrapper">
        <span ng-click=$ctrl.setYesterday()>Вчера</span>
        <span ng-click=$ctrl.setToday()>Сегодня</span>
        <span ng-click=$ctrl.setTwoWeek()>2 недели</span>
        <span ng-click=$ctrl.setMonth()>Месяц</span>
        <span ng-click=$ctrl.setAll()>Все</span>
      </div>
    </div>`
  }
}
