'use strict'

const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: {
        app: [
            './src/app.ts'
        ],
        vendor: [
          'angular/angular.min.js',
          'angular-animate/angular-animate.min.js',
          'angular-aria/angular-aria.min.js',
          'angular-material/angular-material.min.js',
          'moment/min/moment.min.js'
        ]
    },
    context: __dirname + "",
    output: {
        filename: '[name].js',
        path: __dirname + "/dist/",
        sourceMapFilename: 'app.map',
    },
    module: {
        rules: [{
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.html$/,
                use: [{
                        loader: 'file-loader?name=[path][name].[ext]r',
                        options: {
                            minimize: true
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'html-loader'
                    }
                ],
            },
            {
                test: /\.css$/,
                use: [ {
                    loader: "css-loader"
                }]
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([{ from: './src/index.html', to: './index.html' }, {from: './src/style.css', to: './style.css'}])
    ],
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9001
    },
  }
